
//import * as user from './actions';
import * as elements from './elements';

Given("que acesso a aplicação", () => {
	cy.visit('https://15puzzle.netlify.app')
	cy.title().should('be.equal', '15 Puzzle')
});

When("tento mover as pecas", () => {
	cy.get('body').type('{uparrow}{uparrow}{downarrow}{downarrow}{leftarrow}{rightarrow}{leftarrow}{rightarrow}ba')
	cy.wait(2000)
	
});
const resultados = { "é movimentada": '0', "não é movimentada": Cypress.env('pecaEnv') }
Then("as peças são movimentadas", () => {
	cy.screenshot(`Peça é movimentada`);
});
