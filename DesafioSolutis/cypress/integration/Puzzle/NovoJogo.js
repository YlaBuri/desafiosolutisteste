import * as elements from './elements';

Given("que acesso a aplicação", () => {
	cy.visit('https://15puzzle.netlify.app')
	cy.title().should('be.equal', '15 Puzzle')
	cy.wait(3000)
});

When("seleciono o botão de reiniciar", () => {
	cy.get('button[class="Button-sc-1m9brel-0 rdlAW"]').click()
});

Then("um novo jogo se inicia", () => {
	elements.getTempo().should('be.equal', '0s')
	cy.screenshot(`novo jogo`);
});
