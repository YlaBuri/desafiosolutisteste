
const elements = {
    tempo:'div[class="time-container"]',
    pontuacao:'div[class="move-container"]',
    valores: 'div[class="number"]'

}

function getMovimento(){
    return cy.get(elements.pontuacao).invoke('text')
}

function getTempo(){
    return cy.get(elements.tempo).invoke('text')
}

function getValor(valor){
    return cy.get(elements.valores).contains(valor);
}

function transformPosicion(posicao){
    switch(posicao){
        case '1':
            return
    }
}

export{
    getMovimento,
    getTempo,
    getValor
};