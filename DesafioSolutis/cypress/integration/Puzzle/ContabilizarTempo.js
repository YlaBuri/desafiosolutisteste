import * as elements from './elements';

Given("que acesso a aplicação e inicio o jogo ", () => {
	cy.visit('https://15puzzle.netlify.app')
	cy.title().should('be.equal', '15 Puzzle')
	cy.get('body').type('{downarrow}{downarrow}{leftarrow}{leftarrow}{uparrow}{rightarrow}{rightarrow}ba')
	Cypress.env('tempoInicio', elements.getTempo());
});

Then("continou jogando", () => {
	Cypress.env('tempo2', elements.getTempo());
	cy.get('body').type('{leftarrow}{downarrow}{rightarrow}{rightarrow}{leftarrow}ba')
	cy.wait(1000)

	Cypress.env('tempo3', elements.getTempo());
	cy.get('body').type('{leftarrow}{downarrow}{rightarrow}{rightarrow}{leftarrow}ba')
	cy.wait(1000)

	Cypress.env('tempo4', elements.getTempo());
	cy.get('body').type('{leftarrow}{downarrow}{rightarrow}{rightarrow}{leftarrow}ba')
	cy.wait(1000)

	Cypress.env('tempo5', elements.getTempo());
});

Then("o tempo é contabilizado", () => {
	elements.getTempo().should('not.be.equal',Cypress.env('tempo5'))
	elements.getTempo().should('not.be.equal',Cypress.env('tempo4'))
	cy.screenshot(`Contabilizar tenpo`);
});
