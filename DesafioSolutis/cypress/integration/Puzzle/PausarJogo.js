import * as elements from './elements';

Given("que acesso a aplicação e inicio o jogo", () => {
	cy.visit('https://15puzzle.netlify.app')
	cy.title().should('be.equal', '15 Puzzle')
	cy.get('body').type('{uparrow}{uparrow}{downarrow}{downarrow}{leftarrow}{rightarrow}{leftarrow}{rightarrow}ba')
	cy.wait(2000)
	cy.get('body').type('{downarrow}{downarrow}{leftarrow}{leftarrow}{uparrow}{rightarrow}{rightarrow}ba')
});

When("seleciono o botão de pausar", () => {
	cy.get('.Container__PlayPauseContainer-sc-16h46pr-3 > .Button-sc-1m9brel-0').click()
	Cypress.env('tempo', elements.getTempo());
	cy.wait(2000)
});

Then("o jogo é pausado", () => {
	
	const tempoAtual = String(elements.getTempo())
	//tempoAtual.should('be.equal',  String(Cypress.env('tempo')))
	expect(tempoAtual).be.equal(String(Cypress.env('tempo')))
	cy.screenshot(`pausar jogo`);
});
