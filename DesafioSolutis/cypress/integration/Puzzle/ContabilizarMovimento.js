
import * as elements from './elements';

var movimento;
Given("que acesso a aplicação", () => {
	cy.visit('https://15puzzle.netlify.app')
	cy.title().should('be.equal', '15 Puzzle')
});

When("movimento uma peça", () => {
	const movimento = elements.getMovimento()
	Cypress.env('mov1', movimento);
	cy.get('body').type('{uparrow}{uparrow}{downarrow}{downarrow}{leftarrow}{rightarrow}{leftarrow}{rightarrow}ba')
});

Then("o movimento é contabilizado", () => {
	//const movimentoAtual = elements.getMovimento()
	elements.getMovimento().should('not.be.equal', String(Cypress.env('mov1')))
	cy.screenshot(`Contabilizar movimento`);
	//expect(movimentoAtual).to.be.greaterThan(String(cy.env('mov1')))
});
