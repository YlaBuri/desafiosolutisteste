Feature: Jogar Quizz
    COMO jogador
    DESEJO jogar o quizz
    PARA ganhar o máximo de pontos 

    #Regra: É possivel tentar adivinhar a logo
    Scenario Outline: Opção é selecionada
        Given acesso a aplicação
        When seleciono a opção '<opcao>'
        Then a resposta esta '<resultado>'
        Examples:
            |  opcao    |   resultado   |
            |   1       |  <resultado>  |
            |   2       |  <resultado>  |
            |   3       |  <resultado>  |
            |   4       |  <resultado>  |

    # #Regra: Os pontos devem ser contabilizados ao acertar uma resposta 
    # Scenario: Pontos são contabilizados se o jogador acertar
    #     Given acesso a aplicação 
    #     When é selecionada a resposta correta
    #     Then o ponto é adicionado

    #Regra: O tempo deve começar a ser contabilizado, e será pausado quando uma resposta for dada
    # Scenario: Tempo é contabilizado
    #     Given acesso a aplicação
    #     And o tempo é contabilizado
    #     When acerto uma resposta
    #     Then o tempo pausa

    #Regra: Depois de conferir a resposta é possível acessar o link da empresa
    Scenario: Link da empresa é acessado
        Given acesso a aplicação e uma resp é selecionada
        When clico no link da empresa
        Then é redirecionado para o site da empresa

    # #Regra: Ao acertar uma resposta, uma nova logo deve ser disponibilizada ao apertar o botão
    # Scenario: Disponibilizar nova pergunta
    #     Given acesso a página e resposta correta
    #     When aperto no botão
    #     Then uma nova pergunta é disponibilizada

    # #Regra: Ao errar a logo, a partida é encerrada e o jogo pode ser reiniciado
    # Scenario: Pergunta respondida incorretamento
    #     Given acesso a aplicação
    #     When respondo incorretamente uma pergunta
    #     And apertoo botao para iniciar uma nova partida 
    #     Then uma nova partida se inicia

    # #Regra: Ao encerrar a partida, a pontuação pode ser compartilhada no twitter
    # Scenario: Pontuação é compartilhada
    #     Given acesso a aplicação
    #     When a partida é encerrada com o jogador com um ponto ou mais
    #     And a opção de compartilhar é disponibilizada
    #     When seleciono a opção de compartilhar
    #     Then a aplicação redireciona para a página do twitter