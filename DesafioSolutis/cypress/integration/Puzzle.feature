Feature: Jogar o puzzle
    COMO jogador
    DESEJO jogar o puzzle
    PARA me divertir 


    #Regra: É possivel movimentar as peças com as setas do teclado ou clicando sobre a peça que deseja movimentar
    Scenario: Peça é movimentada
        Given que acesso a aplicação 
        When tento mover as pecas 
        Then as peças são movimentadas

    #Regra: Os movimentos devem ser contabilizados
    Scenario: Contabilizar movimento
        Given que acesso a aplicação 
        When movimento uma peça
        Then o movimento é contabilizado

    #Regra: O tempo deve ser contabilizado
    Scenario: Contabilizar tempo
        Given que acesso a aplicação e inicio o jogo 
        And continou jogando
        Then o tempo é contabilizado
    
    #Regra: Um novo jogo pode ser iniciado a qualquer momento
    Scenario: Jogo é reiniciado
        Given que acesso a aplicação 
        When seleciono o botão de reiniciar
        Then um novo jogo se inicia

    #Regra: Um jogo pode ser pausado a qualquer momento depois que a partida inicia
    Scenario: Jogo é pausado
        Given que acesso a aplicação e inicio o jogo 
        When seleciono o botão de pausar
        Then o jogo é pausado

    # #Regra: Quando uma peça estiver no lugar certo ela muda de cor
    # Scenario: Peça é marcada como correta
    #     Given que acesso a aplicação
    #     And movimento uma peça para a posição correta
    #     Then a peça muda de cor

    # #Regra: O jogador ganha quando todas as peças estiverem no lugar
    # Scenario: Encerrar partida
    #     Given que acesso a aplicação
    #     When que todas as peças são colocadas na posição correta
    #     Then a partida é encerrada
        