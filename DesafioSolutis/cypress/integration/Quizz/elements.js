import * as constants from './constants';

const elements = {
    botoes:'//*[@id="root"]/div/div[2]/div[2]/span[2]/div[2]/div/',
    tempo:'',
    pontos: '',
    btnAux:'button[class="Button__StyledButton-aeh8v3-0 iMeQwk animated tada delay-2s infinite"]',
    link:'a[class="Anchor__StyledAnchor-vm2byl-0 dHffsG"]',
    resultado:'h1'
}



function getLink(){
    return cy.get(elements.link)
}

function getBtnAux(){
    return cy.get(elements.btnAux)
}

function getResposta(resposta){
    const resp = transformBtn(Cypress.env('resposta'))
    const x =elements.botoes+resp
    return cy.xpath(x).click()
}

function getResultado(){
    return cy.get(elements.resultado).invoke('text')
}

function transformBtn(resposta){
    switch(resposta){
        case '1':
            return constants.BTN.BTN1 
        case '2':
            return constants.BTN.BTN2 
        case '3':
            return constants.BTN.BTN3 
        case '4':
            return constants.BTN.BTN4 
    }
}

export {
    getLink,
    getBtnAux,
    getResposta,
    getResultado
}