
Given("acesso a aplicação e uma resp é selecionada", () => {
	cy.visit('https://logosweeper.surge.sh/')
	cy.title().should('be.equal', 'Logosweeper')
	cy.xpath('//*[@id="root"]/div/div[2]/div[2]/span[2]/div[2]/div/button[2]').click()
});

When("clico no link da empresa", () => {
	Cypress.env('link', cy.get('h2 > .Anchor__StyledAnchor-vm2byl-0'))
	cy.get('h2 > .Anchor__StyledAnchor-vm2byl-0').click
});

Then("é redirecionado para o site da empresa", () => {
	cy.wait(2000)
	cy.url().should('be.equal', String(Cypress.env('link'))); 
});
