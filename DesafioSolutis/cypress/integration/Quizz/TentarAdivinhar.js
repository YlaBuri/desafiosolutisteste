
import * as elements from './elements';
import * as user from './actions';

Given("acesso a aplicação", () => {
	cy.visit('https://logosweeper.surge.sh/')
	cy.title().should('be.equal', 'Logosweeper')
});


When("seleciono a opção {string}", (resposta) => {
	//elements.getResposta('1').click()
	//user.defineOptions(resposta)
	user.finish(resposta)
	cy.wait(1000)
});

//Congrats!
//GAMEOVER!
const resultados = { "certa": "Congrats!", "errada": "GAMEOVER!" }

Then("a resposta esta {string}", (resultado) => {
	
	elements.getResultado().should('be.equals', String(resultados[resultado]));
	//expect(a).be.equal(resultados[resultado])
	//class="Fieldset__StyledFieldsetContent-sc-1js25js-2 dcYNSw"
	//return true;
});



