
/// <reference types="cypress" />

import * as elements from './elements';

function openPuzzle(){
    cy.visit('https://15puzzle.netlify.app')
}

function defineOptions(resposta) {
    Cypress.env('resposta', resposta);
}

function finish(resposta) {
    Cypress.env('resposta', resposta);
    elements.getResposta(Cypress.env('resposta')).click()
}

export{
    openPuzzle,
    defineOptions,
    finish
}